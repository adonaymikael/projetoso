#include <stdio.h>
#include <sys/types.h>
#include <sys/sem.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
 
#define WAIT_FOR_ENTER  printf( "Pressione ENTER\n" );getchar()
 
struct msgbuf {
        long mtype;         
        char mtext[1];
};
 
int main()
{
        key_t           msgKey;
        int             flag;
        struct msgbuf   buff;
        int             sem;
        int             nRet =0;
 
    printf( "Teste do semaforo - Processo 1. %d\n",flag );
    printf( "Inicio.\n" );
         
    flag = IPC_CREAT|IPC_EXCL;
 
      if( ( msgKey = (key_t) atol( "456789" ) ) <= 0 )
                return 1;
 
      flag |= S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP;
      sem  = (int) msgget( msgKey, flag );
       
            
      if (sem == -1)
             if( errno == EEXIST ) {
                    flag &= ~IPC_EXCL;
                    sem = (int) msgget( msgKey, flag );
                    if (msgctl(sem, IPC_RMID, NULL ) != 0)
                            return 1;
 
                    sem = (int) msgget( msgKey, flag );
                    if (sem == -1)
                            return 1;
             }
             else
                    return 1;
 
      printf( "Semaforo criado. \n" );
 
      WAIT_FOR_ENTER;
 
      buff.mtype = 123;
 
      if( msgsnd( sem, &buff, 1, 0 ) < 0 ){
          return 1;
          }
 
      printf( "Semaforo inicializado. \n" );
      WAIT_FOR_ENTER;
      if( msgrcv( sem, &buff, 1, 0, 0 ) < 0 ){
          return 1;
          }
 
      printf( "Esperando o semaforo. %d\n",flag );
      WAIT_FOR_ENTER;
      msgctl(sem, 0, IPC_RMID );
 
    printf( "Semaforo excluido.\n" );
    printf( "Fim.\n" );
 
    return 0;
}